package com.iftekhar.digitaldr.ui.gallery

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import com.iftekhar.digitaldr.R

class GalleryFragment : Fragment() {
    private var galleryViewModel: GalleryViewModel? = null
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        galleryViewModel = ViewModelProviders.of(this).get(GalleryViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)
        val textView = root.findViewById<TextView>(R.id.text_gallery)
        galleryViewModel!!.text.observe(this, Observer { s -> textView.text = s })
        return root
    }




    class GalleryFragment : AppCompatActivity() {

        lateinit var imageView: ImageView
        lateinit var editText: EditText
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            //setContentView(R.layout.fragment_gallery)

            imageView = findViewById(R.id.imageView)
            editText = findViewById(R.id.editText)
        }


        fun selectImage(v: View) {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1)
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
                imageView.setImageURI(data!!.data)

            }
        }

        fun startRecognizing(v: View) {
            if (imageView.drawable != null) {
                editText.setText("")
                v.isEnabled = false
                val bitmap = (imageView.drawable as BitmapDrawable).bitmap
                val image = FirebaseVisionImage.fromBitmap(bitmap)
                val detector = FirebaseVision.getInstance().onDeviceTextRecognizer

                detector.processImage(image)
                        .addOnSuccessListener { firebaseVisionText ->
                            v.isEnabled = true
                            processResultText(firebaseVisionText)
                        }
                        .addOnFailureListener {
                            v.isEnabled = true
                            editText.setText("Failed")
                        }
            } else {
                Toast.makeText(this, "Select an Image First", Toast.LENGTH_LONG).show()
            }

        }


        private fun processResultText(resultText: FirebaseVisionText) {
            if (resultText.textBlocks.size == 0) {
                editText.setText("No Text Found")
                return
            }
            for (block in resultText.textBlocks) {
                val blockText = block.text
                editText.append(blockText + "\n")
            }
        }


    }


}

